# Unifying Similarity and Unsupervised Categorization
Welcome to our git repository for the Simple Similar Categorization model that we developed for the poster project in the course Computational and Formal Modeling.
You will find our code here that we used to obtain our presented results.

# Formalization of the model
![Formalization of model](https://gitlab.socsci.ru.nl/L.Grootjen/cfmsquad/raw/master/cfm.png "Formalization of model")
