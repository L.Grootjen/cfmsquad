"""Container class defining the standard methods needed for the unsupervised categorisation algorithm."""


class ClusterObject:
    def __init__(self, type="cluster object"):
        self.type = type

    def __eq__(self, other):
        return self.type == other.type

    def __len__(self):
        return 1
