"""This file contains the main tests of the program. """
from Unsupervised_categorization import cluster
import matplotlib.pyplot as plt
from Dot import Dot
from Transformation import Transformation
import pandas as pd
import numpy as np


# Dots domain tests

# # Random dots
# N = 9  # Number of dots
# dots = [Dot(n) for n in range(N)]

# Manual dots
dots = []
d1 = Dot(1)
d1.set_coords(2,4)
dots.append(d1)
d2 = Dot(2)
d2.set_coords(2,6)
dots.append(d2)

d3 = Dot(3)
d3.set_coords(3,8)
dots.append(d3)

d4 = Dot(4)
d4.set_coords(4,8)
dots.append(d4)

d5 = Dot(5)
d5.set_coords(5,6)
dots.append(d5)

d6 = Dot(6)
d6.set_coords(5,4)
dots.append(d6)

d7 = Dot(7)
d7.set_coords(4,2)
dots.append(d7)

d8 = Dot(8)
d8.set_coords(3,2)
dots.append(d8)

column_names = [x.get_id() for x in dots]
dist_arr = np.zeros((len(dots), len(dots)))
sim_scores_arr = np.zeros((len(dots), len(dots)))
eucl_arr = np.zeros((len(dots), len(dots)))

empty_dot = ["0000", "0000"]
sigmoid_param = (1.5, 4)
trans = Transformation(empty_dot, sigmoid_param)
print('Dot input domain:')

# Fill matrices with distances/similarities
for idx, d in enumerate(dots):
    for idx2, d2 in enumerate(dots):
        # Enforce similarity to itself to be one
        if idx == idx2:
            sim_scores_arr[idx, idx2] = 1
            continue
        # Check if the symmetric distance is already calculated
        if sim_scores_arr[idx2, idx] > 0:
            sim_scores_arr[idx, idx2] = sim_scores_arr[idx2, idx]
        else:
            dist = trans.transformation_distance(d, d2)
            sim = trans.similarity(d, d2)
            dist_arr[idx, idx2] = dist
            sim_scores_arr[idx, idx2] = sim
            #print(dist_arr[idx, idx2])
            print('A: ', d.binary_representation[0]+d.binary_representation[1], 'B: ', d2.binary_representation[0]+d.binary_representation[1],
                  'distance: ', round(dist,5) if dist >0 else "0.00000", ' similarity: ', round(sim,5))
            eucl_arr[idx, idx2] = np.sqrt((d.get_x() - d2.get_x())**2 + (d.get_y() - d2.get_y())**2)


# Convert to dataframe
dist_scores = pd.DataFrame(dist_arr, columns=column_names, index=column_names)
similarity_scores = pd.DataFrame(sim_scores_arr, columns=column_names, index=column_names)
eucledian_scores = pd.DataFrame(eucl_arr, columns=column_names, index=column_names)

#print(dist_scores)
print(similarity_scores)
#print(eucledian_scores)

# Cluster the data
clusters, code_len = cluster(dots, similarity_scores, max_iterations=20)
print("Clusters: ", [[(dot.x, dot.y) for dot in cluster]for cluster in clusters], "\nNum clusters: ", len(clusters), "\nCode length:", code_len)


fig = plt.figure()
fig.suptitle("Random dots test")
ax = plt.subplot(111)
for idx, cl in enumerate(clusters):
    ax.scatter([x.get_x() for x in cl], [y.get_y() for y in cl], label="Cluster" + str(idx + 1))
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.show()
