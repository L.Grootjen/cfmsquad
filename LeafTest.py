"""This file contains the main tests of the program. """
from Unsupervised_categorization import cluster
import matplotlib.pyplot as plt
from Dot import Dot
from Transformation import Transformation
import pandas as pd
from Leaf import Leaf
import numpy as np

# Main tests and visualizations


empty_leaf = Leaf(0, "Long", "Long", "Oak", "Sparse")
empty_leaf.binary_representation = ["000", "000", "000", "000"]
sigmoid_param = (1, 4)
trans = Transformation(empty_leaf, sigmoid_param)

leaf1 = Leaf(1, 'Long', 'Medium', 'Oak', 'Sparse')
leaf2 = Leaf(2, 'Medium', 'Long', 'Oak', 'Medium')
leaf3 = Leaf(3, 'Short', 'Medium', 'Olive', 'Sparse')
leaf4 = Leaf(4, 'Medium', 'Short', 'Olive', 'Medium')
leaf5 = Leaf(5, 'Medium', 'Medium', 'Olive', 'Sparse')
leaf6 = Leaf(6, 'Short', 'Short', 'Olive', 'Medium')
leaf7 = Leaf(7, 'Short', 'Long', 'Maple', 'Dense')
leaf8 = Leaf(8, 'Medium', 'Short', 'Maple', 'Medium')
leaf9 = Leaf(9, 'Medium', 'Long', 'Maple', 'Dense')
leaf10 = Leaf(10, 'Medium', 'Medium', 'Maple', 'Medium')

leafs = [leaf1, leaf2, leaf3, leaf4, leaf5, leaf6, leaf7, leaf8, leaf9, leaf10]

distance_leafs = np.zeros((len(leafs), len(leafs)))
similarity_leafs = np.zeros((len(leafs), len(leafs)))
print('Leaf input domain: ')
# Fill matrices with distances/similarities
for idx_a, leaf_a in enumerate(leafs):
    for idx_b, leaf_b in enumerate(leafs):
        # Enforce similarity to itself to be one
        if idx_a == idx_b:
            similarity_leafs[idx_a, idx_b] = 1
            continue
        # Check if the symmetric distance is already computed
        if distance_leafs[idx_b, idx_a] > 0:
            distance_leafs[idx_a, idx_b] = distance_leafs[idx_b, idx_a]
        else:
            dist = trans.transformation_distance(leaf_a, leaf_b)
            sim = trans.similarity(leaf_a, leaf_b)
            print('A: ', leaf_a.get_binary_string(), 'B: ', leaf_b.get_binary_string(), 'distance: ', round(dist,5) if dist >0 else "0.00000", ' similarity: ', round(sim,5))
            distance_leafs[idx_a, idx_b] = dist
            similarity_leafs[idx_a, idx_b] = sim
print(distance_leafs)

leaf_names = [x.get_id() for x in leafs]
sim_leafs_pd = pd.DataFrame(similarity_leafs, columns=leaf_names, index=leaf_names)

# Cluster the data
clusters, code_len = cluster(leafs, sim_leafs_pd, max_iterations=20)
print("Clusters: ", [[leaf.id for leaf in cluster] for cluster in clusters], "\nNum clusters: ", len(clusters), "\nCode length:", code_len)
