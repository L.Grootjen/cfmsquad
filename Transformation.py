import numpy as np
import itertools


class Transformation():

    def __init__(self, empty_representation, sigmoid_param):
        self.transformations = [(self.flip_first_element, self.sample_weights(2)), (self.flip_second_element, self.sample_weights(2)),
                                (self.flip_third_element, self.sample_weights(2)),
                                (self.inverse, 2), (self.rotate, 5),
                                (self.flip_all_0s, 3), (self.flip_all_1s, 3)]
        self.sigmoid_param = sigmoid_param
        self.empty_representation = empty_representation

    def sample_weights(self, weight):
        weight = np.random.normal(weight, weight/2)
        return weight

    def similarity(self, A, B):
        distance = self.transformation_distance(A, B)
        normalization = max(len(A.binary_representation)/ len(B.binary_representation), len(B.binary_representation)/ len(A.binary_representation))
        distance /= normalization
        sim = self.sigmoid(distance)
        return sim

    def sigmoid(self, distance):
        (a, b) = self.sigmoid_param
        return 1/(1 + np.exp(a*(distance-b)))

    def transformation_distance(self, A, B):
        sum_weights_a, sum_weights_b = 0, 0
        transformation_combos = 5

        for domain in A.dictionary.keys():
            solutions = self.get_shortest_domain(A, B, domain, transformation_combos)
            sum_weights_a += min(solutions)

        for domain in B.dictionary.keys():
            solutions = self.get_shortest_domain(B, A, domain, transformation_combos)
            sum_weights_b += min(solutions)

        return max(sum_weights_a, sum_weights_b)


    def get_shortest_domain(self, A, B, domain, combo_size):
        combos = list(itertools.permutations(self.transformations, combo_size))
        solutions = []
        selfIdx, otherIdx = A.get_domain_idx(domain), B.get_domain_idx(domain)
        if A.binary_representation[selfIdx] == B.binary_representation[otherIdx]:
            solutions.append(0)
            return solutions
        else:
            for combo in combos:
                original_me = A.binary_representation[selfIdx]
                combo_sum = 0
                nr = 0
                while nr < len(combo):
                    if A.binary_representation[selfIdx] == B.binary_representation[otherIdx]:
                        solutions.append(combo_sum)
                        break
                    trans, weight = combo[nr]
                    trans(A, domain)
                    combo_sum += weight
                    nr += 1
                A.binary_representation[selfIdx] = original_me


        return solutions

    def flip_first_element(self, object, domain):
        self.flip_element(object, domain, 0)

    def flip_second_element(self, object, domain):
        self.flip_element(object, domain, 1)

    def flip_third_element(self, object, domain):
        self.flip_element(object, domain, 2)

    def flip_element(self, object, domain, idx):
        binary_idx = object.get_domain_idx(domain)
        binary_code = object.binary_representation[binary_idx]
        inverse_bit = self.flip_bit(binary_code[idx])
        new = binary_code[:idx] + inverse_bit + binary_code[idx + 1:]
        object.binary_representation[binary_idx] = new

    def inverse(self, object, domain):
        domain_idx = object.get_domain_idx(domain)
        binary_coding = object.binary_representation[domain_idx]
        new = ''
        for bit in binary_coding:
            new = new + self.flip_bit(bit)
        object.binary_representation[domain_idx] = new

    def flip_bit(self, a):
        return '0' if a == '1' else '1'

    def rotate(self, object, domain):
        domain_idx = object.get_domain_idx(domain)
        binary_code = object.binary_representation[domain_idx]
        new = binary_code[-1:] + binary_code[:-1]
        object.binary_representation[domain_idx] = new

    def flip_all_0s(self, object, domain):
        domain_idx = object.get_domain_idx(domain)
        new = '1'*3
        object.binary_representation[domain_idx] = new

    def flip_all_1s(self, object, domain):
        domain_idx = object.get_domain_idx(domain)
        new = '0'*3
        object.binary_representation[domain_idx] = new
