"""This file contains an implementation of the unsupervised categorization model,
   i.e. the simplicity model by Pothos & Chater"""
import copy
import math


def cluster(Data, d_matrix, max_iterations=300):
    """Clusters the data Data according to a matrix of similarity scores
    for a maximum amount of iterations.
    Codelength = cost & constraints = #data inequalities

    Args:
        Data           : the set of data objects to be clustered. Type: ClusterObject.
        d_matrix       : matrix of similarity scores between objects.
        max_iterations : maximum amount of iterations to continue clustering

    Output:
        clusters : the objects clustered as lists
    """

    clusters = []
    prev_code_length = 0

    # Initial clustering, add all data objects to a new cluster
    for data_object in Data:
        clusters.append([data_object])

    # Iteratively updating the clusters
    for it in range(max_iterations):
        # Merge pairs of clusters and calculate code length
        all_new_pairs = []
        for idx, cl in enumerate(clusters):
            new_pairs_per_cluster = []
            clusters_ex_self = copy.deepcopy(clusters)
            del clusters_ex_self[idx]

            for idx2, cl2 in enumerate(clusters_ex_self):
                # Describe new amount of clusters and the new cluster pair
                new_cluster = copy.deepcopy(cl)
                new_cluster.extend(cl2)

                # Make new data
                n_data = copy.deepcopy(clusters_ex_self)
                del n_data[idx2]
                n_data.append(new_cluster)

                new_prev_code_length = 0
                for cl3 in n_data:
                    new_prev_code_length += get_inequalities(d_matrix, cl3)
                new_pairs_per_cluster.append((new_prev_code_length, new_cluster, cl, cl2))

            all_new_pairs.extend(new_pairs_per_cluster)

        # Check if new pairs have been made
        if len(all_new_pairs) == 0:
            break

        # Get maximum per iteration
        max_pair = all_new_pairs[0]
        for pair in all_new_pairs:
            if pair[0] > max_pair[0]:
                max_pair = pair

        # Update clusters by adding the maximum and removing the subclusters,
        # if this maximum has a higher score than the previous iteration
        if max_pair[0] > prev_code_length:
            clusters.remove(max_pair[2])
            clusters.remove(max_pair[3])
            clusters.append(max_pair[1])
            prev_code_length = max_pair[0]

        print("Iteration: ", it)

    # Calculate end coding result
    tot_codelength = 0
    for cl in clusters:
        tot_codelength += get_inequalities(d_matrix, cl)

    return clusters, tot_codelength


def get_inequalities(d_matrix, cluster):
    """Retrieves the number of inequalities of the objects of a specified cluster.

    Args:
        d_matrix : matrix of similarity scores. Type: pandas dataframe
        cluster  : a list of ClusterObjects
    """
    cluster_names = []
    for co in cluster:
        cluster_names.append(co.get_id())

    non_cluster_names = list(d_matrix.columns.values)
    for x in cluster_names:
        non_cluster_names.remove(x)

    tot_ineqs = 0
    for cb in cluster_names:
        cl_ex_self = copy.deepcopy(cluster_names)
        cl_ex_self.remove(cb)
        for cb2 in cl_ex_self:
            for non_cb in non_cluster_names:
                if d_matrix.loc[cb, cb2] > d_matrix.loc[cb, non_cb]:
                    tot_ineqs += 1

    return math.ceil(tot_ineqs/(len(cluster_names)))
