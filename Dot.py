"""Container class that can be used to cluster the dots."""
import random

from Cluster_object import ClusterObject
import numpy as np


class Dot(ClusterObject):
    def __init__(self, n):
        ClusterObject.__init__(self, "Dot object")
        self.id = n
        self.x = random.randrange(0, 16, 1)
        self.y = random.randrange(0, 16, 1)

        self.convert_to_binary()

        self.dictionary = {'X' : {} ,
                           'Y' : {} }

        for i in range(0, 16):
            self.dictionary.get("X")[i] = self.get_bin(i)
            self.dictionary.get("Y")[i] = self.get_bin(i)


    def __str__(self):
        return "(" + str(self.x) + ", " + str(self.y) + ")"

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def get_id(self):
        return self.id

    def set_coords(self, x, y):
        self.x = x
        self.y = y

    def convert_to_binary(self):
        x_bin = self.get_bin(self.x)
        y_bin = self.get_bin(self.y)
        self.binary_representation = [x_bin, y_bin]

    def get_bin(self, val):
        return format(val, 'b').zfill(4)

    def __eq__(self, other):
        return self.type == other.type and self.x == other.get_x() and self.y == other.get_y()

    def get_domain_var(self, domain):
        if domain == "X":
            return 0
        elif domain == "Y":
            return 1

    def get_domain_idx(self, domain):
        domain_idx = self.get_domain_var(domain)
        return domain_idx
