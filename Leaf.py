"""Container class that can be used to cluster the dots."""
import itertools
import math

from Cluster_object import ClusterObject
import numpy as np


class Leaf(ClusterObject):
    def __init__(self, id, length, width, shape, density):
        ClusterObject.__init__(self, "Leaf object")

        self.id = id
        self.width = width
        self.length = length
        self.shape = shape
        self.density = density
        self.dictionary = {'Length': {'Long': '000', 'Medium': '001', 'Short': '101'},
                           'Width': {'Long': '000', 'Medium': '001', 'Short': '101'},
                           'Shape': {'Oak': '000', 'Maple': '001', 'Olive': '111'},
                           'Density': {'Sparse': '000', 'Medium': '001', 'Dense': '101'}}

        self.binary_representation = self.get_binary_list()


    def get_binary_list(self):
        return [self.dictionary.get('Length').get(self.length),
                self.dictionary.get('Width').get(self.width),
                self.dictionary.get('Shape').get(self.shape),
                self.dictionary.get('Density').get(self.density)]

    def get_binary_string(self):
        return ''.join(self.binary_representation)

    def get_id(self):
        return self.id

    def get_width(self):
        return self.width

    def get_length(self):
        return self.length

    def get_shape(self):
        return self.shape

    def get_density(self):
        return self.density

    def get_domain_var(self, domain):
        if domain == "Length":
            return 0
        elif domain == "Width":
            return 1
        elif domain == "Shape":
            return 2
        elif domain == "Density":
            return 3

    def get_domain_idx(self, domain):
        domain_idx = self.get_domain_var(domain)
        return domain_idx

    def __str__(self):
        return "Leaf " + str(self.id) + ". Width: " + str(self.width) + ", length: " + str(
            self.length) + ", shape: " + self.shape + ", density: " + self.density

    def __eq__(self, other):
        return self.width == other.width and self.length == other.length and self.shape == other.shape and self.density == other.density
